#include "myhttpserver.h"

#include <QObject>
#include <QTcpSocket>
#include <QFile>

#include "myhttpthread.h"

#define _LTLOG_DEBUG(a) _MYLOG_DEBUG(QString("[%1] %2").arg(this->config->getIdentifier()).arg(a))
#define _LTLOG_INFO(a) _MYLOG_INFO(QString("[%1] %2").arg(this->config->getIdentifier()).arg(a))
#define _LTLOG_ERROR(a) _MYLOG_ERROR(QString("[%1] %2").arg(this->config->getIdentifier()).arg(a))

/***************************
 * Constrouctor/Destructor
 ***************************/
MyHttpListener::MyHttpListener(const MHSConfig &aConfig, QObject * widget)
   : QTcpServer(widget)
{
    this->config = new MHSConfig(aConfig);
}

MyHttpListener::~MyHttpListener()
{

}

/***************************
 * static Method
 ***************************/

/***************************
 * setter / getter
 ***************************/

/***************************
 * public
 ***************************/
/** Listen開始
 * @brief MyHttpListenInfo::start
 * @return
 */
bool MyHttpListener::start()
{
    unsigned short port = config->getPort();
    _LTLOG_DEBUG(QString("listen port=%1").arg(port));
    if (this->listen(QHostAddress::Any, port) == false) {
        _LTLOG_ERROR(QString("Start listening failed. code=%1 msg=%2").arg(this->serverError()).arg(this->errorString()));
        return false;
    }

    _LTLOG_INFO(QString("Start listening on %1").arg(port));
    return true;
}

/***************************
 * protected / private
 ***************************/
void MyHttpListener::incomingConnection(qintptr socketDescriptor)
{
    // Socket生成
    _LTLOG_DEBUG(QString("[%1] Accept Connection").arg(socketDescriptor));
    MyHttpThread * thread = new MyHttpThread(socketDescriptor, *this->config, this);
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
}

