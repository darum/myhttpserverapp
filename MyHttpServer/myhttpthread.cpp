#include "myhttpthread.h"

#include <QFile>
#include <QMetaObject>

/**
 * @brief MyHttpThread::MyHttpThread
 * @param aSocketDesc
 * @param aConfig
 * @param parent
 */
MyHttpThread::MyHttpThread(int aSocketDesc, const MHSConfig & aConfig, QObject *parent)
    :QThread(parent), config(aConfig), socketDesc(aSocketDesc)
{
}

/** スレッドメイン
 * @brief MyHttpThread::run
 */
void MyHttpThread::run()
{
    _MYTLOG_DEBUG("Thread Start");

    pSocket = new QTcpSocket();
    if (!pSocket->setSocketDescriptor(this->socketDesc)) {
        _MYTLOG_ERROR("setSocketDescriptor() failed" + pSocket->errorString());

        // Read前は応答なしで終了
        exit(1);
    }

    QMetaObject::Connection conn;
    conn = connect(pSocket, SIGNAL(readyRead()), this, SLOT(readyRead()), Qt::DirectConnection);
    if(!bool(conn)) {
        _MYTLOG_ERROR("SIGNAL-SLOT connect failed: readyRead()");
        exit(1);
    }
    conn = connect(pSocket, SIGNAL(disconnected()), this, SLOT(disconnected()), Qt::DirectConnection);
    if(!bool(conn)) {
        _MYTLOG_ERROR("SIGNAL-SLOT connect failed: disconnected()");
        exit(1);
    }

    exec();
}

void MyHttpThread::readyRead()
{
    _MYTLOG_DEBUG("receive message");
    // 受信
    while(1) {
        QByteArray rawData;
        do {
            rawData = pSocket->readLine();
        } while(rawData.size() == 0);

        QString recvData(rawData);
        recvData = recvData.trimmed();      // 改行をカット

        // リクエストの解析
        bool success = true;
        /*
         * 電文の分解
         */
        QStringList params = recvData.split(" ");
        /*
         * 個数チェック
         */
        if (params.length() != 3) {
            success = false;
        }

        /*
         * HTTPリクエストチェック
         */
        if (success) {
            if(!params[2].contains("HTTP/")) {
                success = false;
            }
        }

        // TODO メソッド

        if (success) {
            _MYTLOG_INFO("Request: " + recvData);

            /*
             * QFile生成
             */
            QString path;
            QTextStream stream(&path);
            stream << this->config.getDocumentRoot() << params[1];
            if (params[1].endsWith("/")) {
                stream << this->config.getDefaultFileName();
            }

            _MYTLOG_DEBUG(QString("path=[%1]").arg(path));
            QFile file(path);
            QByteArray reply;
            unsigned short status = 200;
            if (!file.exists()) {
                QString msg = "404 Not Found";
                reply = msg.toLocal8Bit();
                status = 404;
            } else {
                file.open(QFile::ReadOnly | QFile::Text);
                reply = file.readAll();
            }

            this->sendReply(*pSocket, reply, status);
        } else {
            // パラメータエラー
            QString msg = "Parameter Error";
            QByteArray reply = msg.toLocal8Bit();
            this->sendReply(*pSocket, reply, 400);
        }

        break;
    }

    pSocket->close();

}

void MyHttpThread::sendReply(QTcpSocket &socket, const QByteArray &body, unsigned short status)
{
    /*
     * HTTPヘッダ
     */
    QString header;
    QTextStream stream(&header);
    stream << "HTTP/1.1 " << status << " TODO-Message" << "\n";  // TODO
    stream << "Server: MyHttpServer" << "\n";
    stream << "Content-length: " << body.size() << "\n";
    stream << "\n";
    socket.write(header.toLocal8Bit());

    /*
     * HTTPボディ
     */
    socket.write(body);

    _MYTLOG_INFO(QString("Reply Status=%1").arg(status));
}

void MyHttpThread::disconnected()
{
    _MYTLOG_DEBUG("Thread Terminated");
    pSocket->deleteLater();

    this->exit(0);
}

