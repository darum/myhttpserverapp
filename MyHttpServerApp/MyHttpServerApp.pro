#-------------------------------------------------
#
# Project created by QtCreator 2015-05-22T08:26:27
#
#-------------------------------------------------

QT       += core xml xmlpatterns network

#QT       -= gui

TARGET = MyHttpServerApp
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    ../MyHttpServer/MhsConfig.cpp \
    ../MyHttpServer/mhsconfigholder.cpp \
    ../MyHttpServer/myhttpserver.cpp \
    ../MyHttpServer/myhttpthread.cpp \
    ../MyHttpServer/myhttplistener.cpp \
    ../QLogger/QLogger.cpp

INCLUDEPATH += ../MyHttpServer ../QLogger

#SUBDIRS = ../../MyHttpServer/MyHttpServer

HEADERS += \
    ../MyHttpServer/MhsConfig.h \
    ../MyHttpServer/myhttpserver.h \
    ../MyHttpServer/myhttpserver_global.h \
    ../MyHttpServer/myhttpthread.h \
    ../QLogger/QLogger.h \
    ../MyHttpServer/myhttplogger.h
