#include "myhttpserver.h"

/***************************
 * Constrouctor/Destructor
 ***************************/
MyHttpServer::MyHttpServer(const MHSConfigHolder & aConfigHolder)
{
    foreach(const MHSConfig & config, aConfigHolder.getConfigList()) {
        this->serverInfo.append(new MyHttpListener(config));
    }

    // Logger設定
    QLoggerManager *loggerManager = QLoggerManager::getInstance();
    QStringList modules;
    modules.append(LOGGER_NAME);
    modules.append(THREAD_LOGGER);
    loggerManager->addDestination("console.log", modules, aConfigHolder.getLogLevel());

    QLog_Info(LOGGER_NAME, "Starting Server...");
    QLog_Debug(LOGGER_NAME, QString("Load %1 configurtions has been completed.").arg(serverInfo.size()));
}

MyHttpServer::~MyHttpServer()
{
    foreach(MyHttpListener * pInfo, this->serverInfo) {
        delete pInfo;
    }
}

/***************************
 * static Method
 ***************************/

/***************************
 * setter / getter
 ***************************/

/***************************
 * public
 ***************************/
/** ロード済みのサーバを起動する
 * @brief MyHttpServer::startRequest
 */
void MyHttpServer::startRequest()
{
    int instanceCount = 0;
    foreach(MyHttpListener *pInfo, this->serverInfo) {
        if (pInfo->start()) {
            instanceCount++;
        }
    }

    if (instanceCount == 0) {
        fprintf(stderr, "Server start failed. All ports have never started to listen.¥n");
        exit(-2); // TODO
    }
}


/***************************
 * protected / private
 ***************************/


