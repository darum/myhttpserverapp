#include "MhsConfig.h"

#include <QFile>

#include <QtXml>
#include <QXmlSchema>
#include <QXmlSchemaValidator>

/***************************
 * Constrouctor/Destructor
 ***************************/
MHSConfigHolder::MHSConfigHolder()
    :logLevel(QLogger::InfoLevel)
{
    this->configList = new QList<MHSConfig>();
}

/***************************
 * static Method
 ***************************/

/***************************
 * setter / getter
 ***************************/
const QList<MHSConfig> & MHSConfigHolder::getConfigList() const
{
    return *configList;
}

QLogger::LogLevel MHSConfigHolder::getLogLevel() const
{
    return logLevel;
}

/***************************
 * public
 ***************************/
/** Configのロード
 * @brief MHSConfigHolder::loadConfig
 * @param fileName ファイル名
 * @return true:ロード完了, false:ロード失敗
 */
bool MHSConfigHolder::loadConfig(const QString &fileName)
{
    /*
     * ファイルオープン
     */
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        qDebug() << "XMLファイル読み込みエラー";
        return false;
    }
    QByteArray xmlData = file.readAll();
    file.close();

    /*
     * XMLチェック
     */
    if (!xmlValidate(xmlData)) {
        qWarning() << "XML Validation Error";
        return false;        // 解析しないで終了
    }

    if (!parseXml(xmlData)) {
        qWarning() << "XML Parse Error";
        return false;
    }

    return true;
}

/***************************
 * protected / private
 ***************************/
bool MHSConfigHolder::xmlValidate(const QByteArray &xmlData)
{
    /*
     * XSD読み込み
     */
    QFile schemaFile(XSD_NAME);
    schemaFile.open(QFile::ReadOnly | QFile::Text);
    QByteArray schemaData = schemaFile.readAll();
    schemaFile.close();

    QXmlSchema schema;
    schema.load(schemaData);
    QXmlSchemaValidator validator(schema);
    return validator.validate(xmlData);
}

/**
 * @brief MHSConfigHolder::parseXml
 * @param xmlData
 * @return
 */
bool MHSConfigHolder::parseXml(const QByteArray &xmlData)
{
    QDomDocument doc;
    doc.setContent(xmlData);

    QDomElement root = doc.documentElement();

    // General
    QDomElement generalElement = root.elementsByTagName("General").at(0).toElement();
    if (!this->parseGeneral(generalElement)) {
        return false;
    }

    // Server
    QDomNodeList serverNodes = root.elementsByTagName("Server");
    for (int i = 0; i < serverNodes.size(); ++i) {
        QDomElement server = serverNodes.at(i).toElement();
        MHSConfig config;
        parseServer(server, config);
        this->configList->append(config);
    }

    return true;
}

void MHSConfigHolder::parseServer(const QDomElement &serverElement, MHSConfig &config)
{
    const QDomNodeList nodeList = serverElement.childNodes();

    for(int i = 0; i < nodeList.size(); ++i) {
        QDomElement dataElement = nodeList.at(i).toElement();

        QString name = dataElement.nodeName();
        if (name.length() > 0) {
            QString value = dataElement.text();
            bool ok;
            if (name == "port") {
                unsigned short port = value.toInt(&ok);
                if (ok) {
                    config.setPort(port);
                }
            } else if(name == "maxConnection") {
                int maxConn = value.toInt(&ok);
                if (ok) {
                    config.setMaxConnection(maxConn);
                }
            } else if (name == "documentRoot") {
                config.setDocumentRoot(value);
            } else if (name == "defaultFileName") {
                config.setDefaultFileName(value);
            } else {
                // TODO
            }
        }
    }
}

bool MHSConfigHolder::parseGeneral(const QDomElement &generalElement)
{
    QDomNodeList nodeList = generalElement.childNodes();

    // LogLevel
    QDomElement logLevelElement = nodeList.item(0).toElement(); // TODO 定数
    QString value = logLevelElement.text();
    if (value == "WARNING") {
        this->logLevel = QLogger::FatalLevel;
    } else if (value == "INFO") {
        this->logLevel = QLogger::InfoLevel;
    } else if (value == "DEBUG") {
        this->logLevel = QLogger::DebugLevel;
    } else {
        qWarning() << "Unknown LogLevel=" << value;
        return false;
    }

    return true;
}
