#include <QCoreApplication>
#include <QtGlobal>

#include "MhsConfig.h"
#include "myhttpserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    if (argc != 2) {
        // TODO
        fprintf(stderr, "mhsApp path/to/config\n");
        return 1;
    }

    MHSConfigHolder configHolder;
    if (!configHolder.loadConfig(QString(argv[1]))) {
        qFatal("Server Start failed: config load error.");
        exit(-1);       // Const
    }

    MyHttpServer server(configHolder);
    server.startRequest();

    return a.exec();
}
