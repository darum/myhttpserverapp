#ifndef MYHTTPSERVER_H
#define MYHTTPSERVER_H

#include "myhttpserver_global.h"

#include "MhsConfig.h"
#include "myhttplogger.h"

#include <QList>
#include <QTcpServer>

class MyHttpListener : public QTcpServer
{
    Q_OBJECT
public:
    MyHttpListener(const MHSConfig & aConfig, QObject * widget = NULL);
    ~MyHttpListener();

public:
    bool start();
    bool stop();
    bool status();

protected:
    void incomingConnection(qintptr socketDescriptor);

protected slots:

private:
    MHSConfig * config;

};

// TODO DLL
//class MYHTTPSERVERSHARED_EXPORT MyHttpServer
class MyHttpServer
{

public:
    MyHttpServer(const MHSConfigHolder & aConfigHolder);
    ~MyHttpServer();

    void startRequest();
    void getStatusList(QList<bool> & getStatus);

private:
    QList<MyHttpListener*> serverInfo;
};

#endif // MYHTTPSERVER_H
