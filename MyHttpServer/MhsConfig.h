#ifndef MHSCONFIG_H
#define MHSCONFIG_H

#include <QString>
#include <QList>
#include <QDebug>
#include <QDomElement>

#include "QLogger.h"

#define XSD_NAME "MyHttpServer.xsd"

#define IDENTIFIER_TEMPLATE "ConfigID-%1"

class MHSConfig
{
public:
    MHSConfig();
    MHSConfig(const MHSConfig & origin);
    ~MHSConfig();

    static const unsigned short DEFAULT_HTTP_PORT=80;

public:
    void setPort(unsigned short aPort);
    void setMaxConnection(int aMaxConn);
    void setDocumentRoot(const QString & aRoot);
    void setDefaultFileName(const QString & aDefaultFileName);

    unsigned short getPort() const;
    const QString & getDocumentRoot() const;
    const QString & getDefaultFileName() const;

    const QString & getIdentifier() const;

protected:
    void setIdentifier();

private:
    unsigned short port;
    int maxConnection;
    QString* documentRoot;
    QString* defaultFileName;
    QString* identifier;
};

class MHSConfigHolder
{
public:
    MHSConfigHolder();
private:
    // TODO
    MHSConfigHolder(const MHSConfigHolder & original);

public:
    bool loadConfig(const QString & fileName);

    const QList<MHSConfig> & getConfigList() const;
    QLogger::LogLevel getLogLevel() const;

protected:
    bool xmlValidate(const QByteArray & xmlData);
    bool parseXml(const QByteArray & xmlData);

    bool parseGeneral(const QDomElement &generalElement);
    void parseServer(const QDomElement & serverElement, MHSConfig & config);

private:
    QList<MHSConfig> * configList;

    QLogger::LogLevel logLevel;

};

#endif // MHSCONFIG_H
