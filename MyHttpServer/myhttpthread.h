#ifndef MYHTTPTHREAD_H
#define MYHTTPTHREAD_H

#include <QThread>
#include <QTcpSocket>

#include "MhsConfig.h"

#include "myhttplogger.h"

/** HTTP Requestを処理するスレッド
 * @brief The MyHttpThread class
 */
class MyHttpThread : public QThread
{
    Q_OBJECT
public:
    explicit MyHttpThread(int aSocketDesc, const MHSConfig & aConfig, QObject * parent = NULL);

    void run();

protected:
    void sendReply(QTcpSocket & socket, const QByteArray & body, unsigned short status = 200);

public slots:
    void readyRead();
    void disconnected();

private:
    MHSConfig config;
    int socketDesc;     // Descriptor受取り用
    QTcpSocket* pSocket;    // スレッド内で使用するQTcpSocketオブジェクト

};

// Log
#define _MYTLOG_DEBUG(a)    QLog_Debug(THREAD_LOGGER, QString("[%1] %2").arg(socketDesc).arg(a))
#define _MYTLOG_INFO(a)     QLog_Info(THREAD_LOGGER, QString("[%1] %2").arg(socketDesc).arg(a))
#define _MYTLOG_ERROR(a)    QLog_Error(THREAD_LOGGER, QString("[%1] %2").arg(socketDesc).arg(a))

#endif // MYHTTPTHREAD_H
