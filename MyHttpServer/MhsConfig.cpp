#include "MhsConfig.h"
#include <QDateTime>>
#include <QCryptographicHash>

#define STD_DEFAULT_FILE_NAME "index.html"

/***************************
 * Constrouctor/Destructor
 ***************************/
MHSConfig::MHSConfig()
    : port(DEFAULT_HTTP_PORT), maxConnection(-1), documentRoot(NULL)
{
    this->defaultFileName = new QString(STD_DEFAULT_FILE_NAME);
    setIdentifier();
}

/** コピーコンストラクタ
 * @brief MHSConfig::MHSConfig
 * @param origin
 */
MHSConfig::MHSConfig(const MHSConfig &origin)
    :port(origin.port), maxConnection(origin.maxConnection), documentRoot(new QString(*origin.documentRoot)), defaultFileName(origin.defaultFileName), identifier(origin.identifier)
{
}

MHSConfig::~MHSConfig()
{
    if (documentRoot != NULL) {
        delete documentRoot;
    }
}

/***************************
 * static Method
 ***************************/

/***************************
 * setter / getter
 ***************************/
void MHSConfig::setIdentifier()
{
    this->identifier = new QString(QString(IDENTIFIER_TEMPLATE).arg(QDateTime::currentMSecsSinceEpoch()));
}

void MHSConfig::setPort(unsigned short aPort)
{
    port = aPort;
}

void MHSConfig::setMaxConnection(int aMaxConn)
{
    maxConnection = aMaxConn;
}

void MHSConfig::setDocumentRoot(const QString &aRoot)
{
    if (documentRoot != NULL) {
        delete documentRoot;
    }
    documentRoot = new QString(aRoot);
}

void MHSConfig::setDefaultFileName(const QString &aDefaultFileName)
{
    delete defaultFileName;

    defaultFileName = new QString(aDefaultFileName);
}

unsigned short MHSConfig::getPort() const
{
    return this->port;
}

const QString & MHSConfig::getDocumentRoot() const
{
    return *this->documentRoot;
}

const QString & MHSConfig::getIdentifier() const
{
    return *this->identifier;
}

const QString & MHSConfig::getDefaultFileName() const
{
    return *this->defaultFileName;
}

/***************************
 * public
 ***************************/

/***************************
 * protected / private
 ***************************/

