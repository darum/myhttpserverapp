#ifndef MYHTTPLOGGER_H
#define MYHTTPLOGGER_H

#include <QLogger.h>
using namespace QLogger;

#define LOGGER_NAME "MyHttp"
#define THREAD_LOGGER "MyThread"

#define _MYLOG_DEBUG(a) QLog_Debug(LOGGER_NAME, a)
#define _MYLOG_INFO(a)  QLog_Info(LOGGER_NAME, a)
#define _MYLOG_ERROR(a) QLog_Error(LOGGER_NAME, a)

#endif // MYHTTPLOGGER_H

